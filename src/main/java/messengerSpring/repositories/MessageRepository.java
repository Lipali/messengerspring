package messengerSpring.repositories;


import messengerSpring.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository <Long , Message>{

}
