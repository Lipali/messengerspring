package messengerSpring.repositories;

import org.apache.tomcat.jni.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Long , User> {

        User findUserByName(String name);
}
