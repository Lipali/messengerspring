package messengerSpring.model.entity;

import lombok.*;
import messengerSpring.model.enums.Status;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "messages")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String consignor;
    private String recipient;
    private Status status ;
    private String text;
}
