package messengerSpring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MessengerSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(MessengerSpringApplication.class, args);
	}

}
